" Color scheme
set termguicolors
set background=dark
colorscheme solarized

syntax enable

set number relativenumber

inoremap <A-h> <Esc>
inoremap <A-o> <Esc>o
inoremap <A-O> <Esc>O

"Finding files
"Search down into subfolders
set path+=**
"Display all matching files when using tab completion

"ctags: create the tags file
command! MakeTags !ctags -R

" FILE BROWSING WITH NETRW 
" Enable syntax and plugins (for netrw)
filetype plugin on
" let g:netrw_banner=0		" disable banner
let g:netrw_browse_split=4 " open in prior window
let g:netrw_altv=1 " open splits to the right
let g:netrw_liststyle=3 " tree view

" Mantain undo history between sessions
set undofile
set undodir=~/.vim/undodir

