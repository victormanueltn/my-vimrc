set nocompatible
filetype off

set termguicolors

" set the runtime path to include Vundle and initialize
set rtp+=$HOME/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
      Plugin 'VundleVim/Vundle.vim'
      Plugin 'Valloric/YouCompleteMe'
      Plugin 'jacoborus/tender.vim'
      Plugin 'morhetz/gruvbox'

call vundle#end() " required
filetype plugin indent on " required

set background=dark
colorscheme solarized8_high
let g:solarized_extra_hi_groups=1
let g:solarized_termtrans=1

"hybrid (absolute and relative) line numbers
set number relativenumber
augroup numbertoggle
	autocmd!
	autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
	autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" Toggling paste mode so that Vim does not autoindent when pasting from
" clipboard
set pastetoggle=<F2>

set wildmenu

" Highlighting current line
set cursorline

" Vertical line at 80th column
set colorcolumn=80

" Disabling beep
set visualbell

" Turning tabs into spaces
set expandtab
" Tab length
set tabstop=4
" Indentation length
set shiftwidth=4

" Maintaining undo history between sessions
set undofile
set undodir=~/.vim/undodir
set undolevels=200
set undoreload=10000

"" YouCompleteMe configuration
" Global ycm_extra_conf.py
let g:ycm_global_ycm_extra_conf = "~/.vim/bundle/YouCompleteMe/.my_ycm_extra_conf.py"

" No preview window for autocomplete:
set completeopt-=preview

" Making it possible to change from modified buffer
set hidden

" Search down into subfolders
set path+=**

autocmd VimEnter * execute "normal \<C-L>"

fu! SaveSession()
    execute 'mksession! $HOME/.vim/sessions/session.vim'
endfunction

fu! LoadSession()
    execute 'source $HOME/.vim/sessions/session.vim'
endfunction

autocmd VimEnter * call LoadSession()
autocmd VimLeave * call SaveSession()


"" File browsing with netrw
filetype plugin on
" let g:netrw_banner=0 " disable banner
let g:netrw_altv=1 " open splits to the right
let g:netrw_liststyle=3 " tree view
